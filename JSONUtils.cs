﻿using System;
using System.Web.Script.Serialization;

namespace JSONUtilities
{
    public static class JSONUtils 
    {

        public static T Deserialize<T>(string jsonString)
        {
            
            var jsonSerializer = new JavaScriptSerializer();

            var deserializedObject
                = jsonSerializer.Deserialize<T>(jsonString);

            return deserializedObject;

        }


        public static string Serialize(Object objToSerialize)
        {
            
            var jsonSerializer = new JavaScriptSerializer();

            string serializedString
                = jsonSerializer.Serialize(objToSerialize);

            return serializedString;
            
        }
    }
}
